yum -y install git
mkdir -p /var/www/html
cd /var/www/html
git clone https://gitlab.com/taylor.r/lafrance.git
chmod -R 700 lafrance
cd lafrance
./provision-non-vagrant.sh
